import binascii
import sys

def getCaracValue(carc):
    Dico = ["0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    temp = 0
    if carc == "<" :
        return 0
    else:
        for c in Dico :
            if c == carc :
                return temp
            else:
                temp +=1

def calculKey(Chaine) :
    Weight = [7,3,1]
    Som = 0
    Temp = 0
    for carac in Chaine :
        Som = Som + (getCaracValue(carac) * Weight[Temp%3]) 
        Temp += 1
    Key = Som % 10
    return Key

def calculSomKey() :
    txtFile = open("data.txt", 'r')
    chaine = txtFile.read()
    som = 0
    for temp in range(0,200) :
        som = som + calculKey(chaine[0+(temp*10):9+(temp*10)])
    return som

if __name__ == "__main__":

    print(calculSomKey())
        
