def testChiffreEntier(nb):
    if ( nb % 1 == 0):
        return True
    else:
        return False

def askCountry(): 
    country = input("Quel est votre pays d'origine ?")
    country = country[0:3].upper()
    return country

def askFirstName():
    FirstName = input("Quel est votre prénom ?")
    if len(FirstName)>14 :
        FirstName = FirstName[0:14].upper()
    else :
        FirstName = FirstName.upper() + addChevron(14- len(FirstName))
        return FirstName

def askLastName():
    LastName = input("Quel est votre nom?")
    if len(LastName)>25 :
        LastName = LastName[0:25].upper()
        return LastName
    else :
        LastName = LastName.upper() + addChevron(25- len(LastName))
        return LastName

def addChevron(nb) :
    chaine = ""
    for temp in range(0,nb):
        chaine = chaine + '<'
    return chaine

def askDateOfBirth() :
    Year = input("Quelle est votre année de naissance ?")
    Year = Year[2:]
    Year = str(Year)

    Month = str(askMonth())
    if len(Month) == 1 :
        Month = "0" + str(Month)

    Day = str(askDay())
    if len(Day) == 1 :
        Day = "0" + str(Day)

    DateOfBirth = Year + Month + Day
    return DateOfBirth

def askMonth():
    Month = input("Quelle est votre mois de naissance ?")
    if (int(Month) >=1 and int(Month) <=12) :
        return Month
    else:
        askMonth()

def askDay():
    Day = input("Quelle est votre jour de naissance ?")
    if (int(Day) >=1 and int(Day) <=31) :
        return Day
    else:
        askDay()

def askNumberSiteEmetteur() :
    NumberSiteEmetteur = input("Quelle est le numéro de site émetteur ?")
    if len(str(NumberSiteEmetteur)) == 6 :
        return NumberSiteEmetteur
    else :
        askNumberSiteEmetteur()

def askSexe() :
    sexe = input("Quelle est votre sexe? Merci de taper F pour Female et M pour Male")
    return sexe.upper()

def calculDateOfBirthKey():
    DateOfBirth = askDateOfBirth()
    Weight = [7,3,1,7,3,1]
    som = 0
    for temp in range(0,6) :
        som = som + int(DateOfBirth[temp])*Weight[temp]
    Key = som % 10
    DateOfBirthKey = DateOfBirth + str(Key)
    return DateOfBirthKey

def calculIDNumberKey():
    IDNumber = "210178310169"
    Weight = [7,3,1,7,3,1,7,3,1,7,3,1]
    som = 0
    for temp in range(0,12) :
        som = som + int(IDNumber[temp])*Weight[temp]
    Key = som % 10
    IDNumberKey = IDNumber + str(Key)
    return IDNumberKey

def calculFinalKey() :
    Chaine = concatFinal()
    Weight = [7,3,1]
    Som = 0
    Temp = 0
    for carac in Chaine :
        Som = Som + (getCaracValue(carac) * Weight[Temp%3]) 
        Temp += 1
    Key = Som % 10
    return Chaine + str(Key)

def testTemp(nb) :
    Weight = [7,3,1]
    return Weight[nb%3]

def getCaracValue(carc):
    Dico = ["0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    temp = 0
    if carc == "<" :
        return 0
    else:
        for c in Dico :
            if c == carc :
                return temp
            else:
                temp +=1

def concatFirstRow():
    result = "ID" + askCountry() + askLastName() + askNumberSiteEmetteur()
    return result

def concatSeconRow():
    result = calculIDNumberKey() + askFirstName() + calculDateOfBirthKey() + askSexe()
    return result

def concatFinal() :
    result = concatFirstRow() + concatSeconRow()
    return result

def createMRZ():
    return calculFinalKey()

if __name__ == "__main__":
    print(createMRZ())