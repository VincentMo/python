from ast import Param
from itertools import combinations
import math
import re
import itertools


def testChiffreEntier(nb):
    if ( nb % 1 == 0):
        return True
    else:
        return False

def additionCarreChiffre(nb):
    nombres = []
    while nb > 0:
        nombres.append(nb % 10)
        nb = nb // 10
    som = 0
    for nombre in nombres:
        som += nombre**2
    return som

#EXERCICE 1
def chiffrePorteBonheur(nb):
    temp = nb
    if (testChiffreEntier(nb)):
        while nb > 10 :
            nb = additionCarreChiffre(nb)
        if(nb == 1):
            print(temp , "est un nombre porte bonheur")
        else:
            print(temp , "n'est pas un nombre porte bonheur 2")
    else:
        print(temp , "n'est pas un nombre porte bonheur 1")

# EXERCICE 2
def généIni():
    return list(map(chr, range(97, 123)))

def duplicateur(nb):
    alphabet = généIni()
    newAlphabet =[]
    for letter in alphabet:
        for _ in range(nb): newAlphabet.append(letter)
    print(newAlphabet)

# EXERCICE 3
def powerset(param):
    all_combi = []
    for temp in range(len(param) +1):
        combi= itertools.combinations(param, temp)
        combi_list = list(combi)
        all_combi += combi_list
    print(all_combi)

# EXERCICE 4

if __name__ == "__main__":
    print("Main")
    # chiffrePorteBonheur(913)
    # powerset([1,2,3])
    # duplicateur(3)
    # https://open.spotify.com/track/4uLU6hMCjMI75M1A2tKUQC?si=r3VuPN55QK2gx0IB7s_8fw