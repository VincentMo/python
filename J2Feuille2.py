import random
import math
import itertools
from itertools import zip_longest

# EXERCICE 1
class Domino():
    def __init__(self):
        self.FaceA = random.randrange(0,6,1)
        self.FaceB = random.randrange(0,6,1)


    def affiche_points(self):
        print("Face A:", self.FaceA , " Face B :" , self.FaceB)

    def totale(self):
        return self.FaceA+self.FaceB

    def __repr__(self):
        return "Face A: {self.FaceA}, Face B: {self.FaceB}".format(self=self)

# EXERCIE 2
class CompteBancaire():
    def __init__(self,nom,solde,):
        self.nom = nom
        self.solde = solde
    
    def depot(self, somme):
        self.solde += somme

    def retrait(self, somme):
        self.solde -= somme
    
    def affiche(self):
        print("Le solde du compte de ", self.nom , " est de ", self.solde)

    def __repr__(self):
        return "Le solde du compte de {self.nom} est {self.solde}".format(self=self)

# EXERCICE 3
class TableMultiplication():
    def __init__(self, nb):
        self.itéra = 0
        self.nb = nb

    def prochain(self):
        print(self.nb * self.itéra)
        self.itéra += 1

    def __repr__(self):
        return "Itérateur: {self.itéra}, Nombre: {self.nb}".format(self=self)

# EXERCICE 4
class Fraction():
    def __init__(self):
        self.num = random.randrange(0,99,1)
        self.denum = random.randrange(1,99,1)

    def seter(self,num, denum):
        self.num = num
        self.denum = denum
    
    def affiche(self):
        print(self.num, "/", self.denum)

    def reduc(self):
        pgcd = math.gcd(self.num, self.denum)
        print("PGCD :", pgcd)
        print("Fraction minimisé :", int(self.num/pgcd) , "/", int(self.denum/pgcd))

    def __add__(self , other):
        newfrac = Fraction()
        newfrac.num = self.num * other.denum + other.num * self.denum
        newfrac.denum = self.denum * other.denum
        pgcd = math.gcd(newfrac.num, newfrac.denum)
        newfrac.num = int(newfrac.num / pgcd)
        newfrac.denum = int(newfrac.denum / pgcd)
        return newfrac

    def __sub__(self, other):
        newfrac = Fraction()
        newfrac.num = self.num * other.denum - other.num * self.denum
        newfrac.denum = self.denum * other.denum
        pgcd = math.gcd(newfrac.num, newfrac.denum)
        newfrac.num = int(newfrac.num / pgcd)
        newfrac.denum = int(newfrac.denum / pgcd)
        return newfrac

    def __mul__(self, other):
        newfrac = Fraction()
        newfrac.num = self.num * other.num
        newfrac.denum = self.denum * other.denum
        pgcd = math.gcd(newfrac.num, newfrac.denum)
        newfrac.num = int(newfrac.num / pgcd)
        newfrac.denum = int(newfrac.denum / pgcd)
        return newfrac

    def __truediv__(self, other):
        newfrac = Fraction()
        newfrac.num = self.num * other.denum
        newfrac.denum = self.denum * other.num
        pgcd = math.gcd(newfrac.num, newfrac.denum)
        newfrac.num = int(newfrac.num / pgcd)
        newfrac.denum = int(newfrac.denum / pgcd)
        return newfrac

    def __repr__(self):
        return "Numérateur: {self.num}, Denominateur: {self.denum}".format(self=self)

# EXERCICE 6

class Poly():
    def __init__(self, *args):
        temp = []
        temp2 = args
        for arg in temp2:
            temp.append(arg)
        self.coeff = temp
    
    def affiche(self):
        print(self.coeff)

    def evalue(self,nb):
        som = 0
        for i, value in enumerate(self.coeff, 1):
            som += value**i
        print("Res =",som)

    def __add__(self, other):
        newpoly = Poly()
        listeTupleCoefficient = list(zip_longest(self.coeff,other.coeff, fillvalue=0))
        for i,tupl in enumerate(listeTupleCoefficient, 1):
            newpoly.coeff.append(0)
            for val in tupl:
                newpoly.coeff[i-1] += val
        return newpoly

    def __mul__(self, other):
        print("Je suis en week end j'ai besoin d'un mojito excusez moi")
        return sorry


if __name__ == "__main__":
    print("Phase test")
    # print("Exo 1")
    # dom = Domino()
    # dom.affiche_points()
    # print(dom.totale())
    # print("Exo 2")
    # CompteBill = CompteBancaire("Bill", 1500)
    # CompteBill.affiche()
    # CompteBill.depot(1000)
    # CompteBill.affiche()
    # CompteBill.retrait(500)
    # CompteBill.affiche()
    # print("Exo 3")
    # tab9 = TableMultiplication(9)
    # tab9.prochain()
    # tab9.prochain()
    # tab9.prochain()
    # print("Exo 4")
    # frac = Fraction()
    # frac.affiche()
    # frac.reduc()
    # frac2 = Fraction()
    # frac2.affiche()
    # frac2.reduc()
    # frac3 = frac2 - frac
    # frac3.affiche()
    # frac2.affiche()
    # print("Exo 5")
    # frac = Fraction()
    # print(frac)
    print("Exercice 6")
    poly1 = Poly(-3, 5, 2)
    poly1.affiche()
    poly1.evalue(5)
    poly2 = Poly(1, 6, 9,10, 3)
    poly2.affiche()
    poly3 = poly1 + poly2
    poly3.affiche()
